using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo  
{

    class Animal   //Clase Base (padre)
    {
        public virtual void animalSound()
        {
            Console.WriteLine("Los animales hacen un sonido");

        }
    }
        class Pig : Animal  //Clase derivada (hijo)
        {
            public override void animalSound()
            {
                Console.WriteLine("El cerdo hace wik wik");

            }
        }

        class Dog : Animal  //Clase derivada (hijo)
        {
            public override void animalSound()
            {
                Console.WriteLine("El perro hace wuaf wuaf");

            }
        }

        class Cat : Animal  //Clase derivada (hijo)
        {
            public override void animalSound()
            {
            Console.WriteLine("El gato hace miau miau");

            }
        }
    
    class Program
    {
        static void Main(string[] args)
        {

            Animal myAnimal = new Animal();  // Creando el objeto animal
            Animal myPig = new Pig();  // Creando el objeto pig
            Animal myDog = new Dog();  // Creando el objeto dog
            Animal myCat = new Cat();

            myAnimal.animalSound();
            myPig.animalSound();
            myDog.animalSound();
            myCat.animalSound();


            
            Console.WriteLine(myAnimal);
            Console.Read();
        }
    }
}
